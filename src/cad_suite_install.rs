use std::env;
use std::fmt::Write;
use std::path::{Path, PathBuf};

use bytes::{Bytes, BytesMut};
use camino::Utf8PathBuf;
use color_eyre::eyre::{bail, Context, Result};
use flate2::bufread::GzDecoder;
use futures::StreamExt;
use indoc::indoc;
use log::{debug, info};
use reqwest::header::CONTENT_LENGTH;
use serde::Deserialize;
use tar::Archive;

use crate::cmdline::Args;

#[allow(unused)] // NOTE: unusued while waiting for fixed cad suite
#[derive(Deserialize, Debug)]
struct GithubReleaseAsset {
    name: String,
    browser_download_url: String,
}

#[allow(unused)] // NOTE: unusued while waiting for fixed cad suite
#[derive(Deserialize, Debug)]
struct GithubReleases {
    assets: Vec<GithubReleaseAsset>,
}

fn install_path() -> Result<PathBuf> {
    let dirs = xdg::BaseDirectories::new()?;

    Ok(dirs.get_data_file("swim/bin"))
}

pub fn has_oss_cad_suite() -> Result<bool> {
    Ok(install_path()?.exists())
}

pub fn bin_path() -> Result<PathBuf> {
    Ok(install_path()?.join("oss-cad-suite/bin"))
}

pub fn cad_suite_lib_path() -> Result<Utf8PathBuf> {
    Ok(install_path()?.join("oss-cad-suite/lib").try_into()?)
}

pub async fn install_oss_cad_suite(reinstall: bool) -> Result<()> {
    if has_oss_cad_suite()? {
        if !reinstall {
            info!("Tools are already installed. Used --reinstall if you want to update");
            return Ok(());
        } else {
            std::fs::remove_dir_all(install_path()?)
                .context("Failed to remove previous install")?;
        }
    }

    if std::env::consts::ARCH == "x86_64" && std::env::consts::OS == "linux" {
        let dirs = xdg::BaseDirectories::new()?;

        let install_dir = dirs
            .create_data_directory("swim/bin")
            .context("Failed to create ${XDG_DATA_HOME}/swim/bin")?;

        let client = reqwest::ClientBuilder::new()
            .user_agent("swim")
            .build()
            .context("Failed to build HTTP client")?;

        // lifeguard: https://gitlab.com/spade-lang/swim/-/issues/73
        // let release_url =
        //     "https://api.github.com/repos/YosysHQ/oss-cad-suite-build/releases/latest";

        // info!("Requesting list of releases");

        // let release_list_body = client
        //     .get(release_url)
        //     .send()
        //     .await
        //     .context("Failed to build request")?
        //     .text()
        //     .await
        //     .with_context(|| format!("Failed to get body from {release_url}"))?;

        // debug!("Release list body:\n{release_list_body}");

        // let release_list = serde_json::from_str::<GithubReleases>(&release_list_body)
        //     .with_context(|| {
        //         format!("Failed to decode github release list body from {release_url}")
        //     })?;

        // let relevant_download = release_list
        //     .assets
        //     .into_iter()
        //     .find(|asset| asset.name.starts_with("oss-cad-suite-linux-x64"))
        //     .ok_or_else(|| anyhow!("Did not find a oss-cad-suite-linux-x64 release"))?;

        // let url = relevant_download.browser_download_url;
        let url = "https://github.com/YosysHQ/oss-cad-suite-build/releases/download/2023-12-31/oss-cad-suite-linux-x64-20231231.tgz";
        info!(
            "Downloading {} from {}",
            /*relevant_download.name,*/ "oss-cad-suite-linux-x64-20231231.tgz", url
        );

        let tar_bytes;
        let request = client
            .get(url)
            .send()
            .await
            .with_context(|| format!("Failed to connect to {url}"))?;
        match request.headers().get(CONTENT_LENGTH) {
            Some(total_bytes) => {
                let len = total_bytes
                    .to_str()
                    .expect("invalid Content-Length")
                    .parse()
                    .expect("invalid Content-Length");
                let pb = indicatif::ProgressBar::new(len);
                pb.set_style(indicatif::ProgressStyle::with_template("{spinner:.green} [{elapsed_precise}] [{wide_bar:.cyan/blue}] {bytes}/{total_bytes} ({eta})")
                    .unwrap()
                    .with_key("eta", |state: &indicatif::ProgressState, w: &mut dyn Write| write!(w, "{:.1}s", state.eta().as_secs_f64()).unwrap())
                    .progress_chars("#>-"));
                let mut buf = BytesMut::with_capacity(len as usize);
                let mut stream = request.bytes_stream();
                while let Some(item) = stream.next().await {
                    let item = item.with_context(|| format!("Reading response from {url}"))?;
                    let bytes = item.len();
                    pb.inc(bytes as u64);
                    buf.extend(item);
                }
                tar_bytes = Bytes::from(buf);
            }
            None => {
                tar_bytes = request
                    .bytes()
                    .await
                    .with_context(|| format!("Failed to get bytes from {url}"))?;
            }
        }

        info!("Unpacking archive");
        let mut archive_content = Archive::new(GzDecoder::new(tar_bytes.as_ref()));

        for entry in archive_content
            .entries()
            .context("Failed to get entries in tar archive")?
        {
            let mut file = entry.context("Faield to get entry from tar archive")?;

            let header = file.header().clone();
            let filename = header.path().context("Failed to get filename")?;

            file.unpack_in(&install_dir)
                .with_context(|| format!("Failed to write {filename:?} to {:?}", install_dir))?;
        }

        info!("Patching tabbypy3");
        let tabbypy3 = install_dir.join("oss-cad-suite/bin/tabbypy3");
        patch_tabbypy(&tabbypy3).context("Failed to patch tabbypy")?;

        Ok(())
    } else {
        bail!("Automatic OSS cad suite install is only supported on linux x64. For other platforms you can install it manually from https://github.com/YosysHQ/oss-cad-suite-build and add the tools to $PATH")
    }
}

/// oss-cad-sutite bundles a python3.8 environment which we have to use if we run verilator or
/// icarus with cocotb from the cad-suite. However, it does not play nice with venvs.
/// Running it in a venv created by the system python fails with glibc errors, and the python
/// wrapper that cad-suite ships with messes with some environment variables in a way that
/// makes it impossible to use with virtual environments out of the box.
///
/// This is reported in https://github.com/YosysHQ/oss-cad-suite-build/issues/80 but the solution
/// I have found is a bit too hacky to upstream and for general use for now, so we'll patch
/// that script ourselves
// lifeguard https://github.com/YosysHQ/oss-cad-suite-build/issues/80
fn patch_tabbypy(file: &Path) -> Result<()> {
    let patched_script = indoc!(
        r#"
        #!/usr/bin/env bash
        release_bindir="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
        release_bindir_abs="$(readlink -f "$release_bindir")"
        release_topdir_abs="$(readlink -f "$release_bindir/..")"
        export PATH="$release_bindir_abs:$PATH"
        export PYTHONEXECUTABLE="${BASH_SOURCE[0]}"
        export PYTHONNOUSERSITE=1
        export SSL_CERT_FILE="$release_topdir_abs"/etc/cacert.pem
        exec "$release_topdir_abs"/lib/ld-linux-x86-64.so.2 --inhibit-cache --inhibit-rpath "" --library-path "$release_topdir_abs"/lib "$release_topdir_abs"/libexec/python3.8 "$@""#
    );
    // We'll check the script we downloaded against the one this was patched on top of in order to
    // not patch a script that has been changed upstream. This should be better than
    // patching over upstream fixes, though it will mean that users will be stuck
    // until we fix things
    let original_script = indoc!(
        r#"
            #!/usr/bin/env bash
            release_bindir="$(dirname "${BASH_SOURCE[0]}")"
            release_bindir_abs="$(readlink -f "$release_bindir")"
            release_topdir_abs="$(readlink -f "$release_bindir/..")"
            export PATH="$release_bindir_abs:$PATH"
            export PYTHONEXECUTABLE="$release_topdir_abs/bin/tabbypy3"
            export PYTHONHOME="$release_topdir_abs"
            export PYTHONNOUSERSITE=1
            export SSL_CERT_FILE="$release_topdir_abs"/etc/cacert.pem
            exec "$release_topdir_abs"/lib/ld-linux-x86-64.so.2 --inhibit-cache --inhibit-rpath "" --library-path "$release_topdir_abs"/lib "$release_topdir_abs"/libexec/python3.8 "$@"
        "#
    );

    let current_countent =
        std::fs::read_to_string(file).with_context(|| format!("Failed to read {file:?}"))?;
    if current_countent != original_script {
        bail!("The oss-cad-suite python script has changed upstream.\n Please open an issue for us to patch swim at https://gitlab.com/spade-lang/swim");
    }

    std::fs::write(&file, patched_script)
        .with_context(|| format!("Failed to write patched tabbypy3 to {file:?}"))?;

    Ok(())
}

/// If oss-cad-suite is installed, this will set up $PATH so all child processes
/// of swim will use programs from oss-cad-suite over other programs
pub fn maybe_use_oss_cad_suite(args: &Args) -> Result<()> {
    if has_oss_cad_suite()? && !args.use_system_tools {
        debug!("Using OSS cad suite from {:?}", bin_path()?);
        if let Some(path) = env::var_os("PATH") {
            let paths = env::split_paths(&path);
            let joined = env::join_paths([bin_path()?].into_iter().chain(paths))?;
            env::set_var("PATH", joined)
        } else {
            env::set_var("PATH", &install_path()?);
        }
    } else {
        if args.use_system_tools {
            debug!("Using system-wide cad tools because --use-system-tools was specified",);
        } else {
            debug!(
                "Using system-wide cad tools because no tools were found in {:?}",
                install_path()
            );
        }
    }
    debug!("Set PATH to {:?}", env::var_os("PATH"));
    Ok(())
}
