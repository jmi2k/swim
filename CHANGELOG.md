# Changelog

All notable changes to this project will be documented in this file.

Spade is currently unstable and all 0.x releases are expected to contain
breaking changes. Releases are mainly symbolic and are done on a six-week
release cycle. Every six weeks, the current master branch is tagged and
released as a new version.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

### Added

- [!126][!126] Swim can now run cocotb tests on more operating systems than before

### Changed

- [!131][!131] Set `main` as the default branch for git dependencies and `test` as the default `testbench_dir` for simulation.

[!126]: https://gitlab.com/spade-lang/swim/-/merge_requests/126

## [0.8.0] - 2024-05-14

### Added

- [!127][!127] Added an `ASSERT` macro to the Verilator wrapper which doesn't terminate before generating waves
- [!127][!127] Add `--bear` to `swim test` which will generate a `compile_commands.json` which provides LSP diagnostics in Verilator tests. **Known issue** This does not work when running `oss-cad-suite`. Run `swim test --use-system-tools` to work around this problem.

### Fixed

- [!127][!127] Fix waves not being generated when failing `assert_eq`


[!127]: https://gitlab.com/spade-lang/swim/-/merge_requests/127

## [0.7.0] - 2024-03-21

### Added

- [!120][!120] Add `--use-sudo` to the upload command which will run the upload tool as superuser.
- [!121][!121] `swin clean` does not remove the Spade build directory by
  default. Run `swim clean --and-spade` to remove it.

### Changed

- [!119][!119] Lock oss-cad-suite to 2023-12-31, hopefully temporarily. See [#73][#73]

[!119]: https://gitlab.com/spade-lang/swim/-/merge_requests/119
[#73]: https://gitlab.com/spade-lang/swim/-/issues/73
[!120]: https://gitlab.com/spade-lang/swim/-/merge_requests/120
[!121]: https://gitlab.com/spade-lang/swim/-/merge_requests/121

## [0.6.0]

### Fixed

- [!116][!116] Exit with failure if finding the top module in a test file fails

[!116]: https://gitlab.com/spade-lang/swim/-/merge_requests/116

## [0.5.0]

### Added


- [!99][!99] Verilator can now be used for Simulation
- [!101][!101] Add `swim install-tools` to install https://github.com/YosysHQ/oss-cad-suite-build for use with swim. After this is used, swim will use those tools over system-wide tools when available.
- [!109][!109] Added `--use-system-tools` flag to use the tools from the system install even if install-tools has been used.
- [!107][!107] You can now use glob syntax in the `extra_verilog` lists
- Added documentation for the [swim.toml](https://docs.spade-lang.org/swim_project_configuration/config__Config.html) and [swim-plugin.toml](https://docs.spade-lang.org/swim_project_configuration/plugin__config__PluginConfig.html) configuration files


### Fixed

- [!112][!112] Prevent maturin from rebuilding every time even if no changes occurred

### Changed

- [!105][!105] Make VCD translation opt-in. The preferred way to get translated
  waveforms is now with https://gitlab.com/surfer-project/surfer, but if you
  still want to use gtkwave, you can request translation with `--translate-vcd`

[!99]: https://gitlab.com/spade-lang/swim/-/merge_requests/99
[!101]: https://gitlab.com/spade-lang/swim/-/merge_requests/101
[!105]: https://gitlab.com/spade-lang/swim/-/merge_requests/105
[!107]: https://gitlab.com/spade-lang/swim/-/merge_requests/107
[!109]: https://gitlab.com/spade-lang/swim/-/merge_requests/109
[!112]: https://gitlab.com/spade-lang/swim/-/merge_requests/112

## [0.4.0]

### Added

- [!94][!94] Tests are now run in parallel, and all test functions are run in individual cocotb contexts. Use the `-j` flag to specify how many parallel tasks to run.
- [!94][!94] A new environment variable `SWIM_ROOT` containing the absolute path to the swim project is set for tests
- [!98][!98] Add clickable links to open the VCD file from each test in gtkwave or surfer. Support for this must be set up by running `swim setup-links`

### Fixed

### Changed

- [!95][!95] *Breaking change* Add a library name field to `swim.toml`
- [!95][!95] Ensure that dependency names match the name specified in the dependency

### Removed

### Internal


[!94]: https://gitlab.com/spade-lang/swim/-/merge_requests/94
[!95]: https://gitlab.com/spade-lang/swim/-/merge_requests/95
[!98]: https://gitlab.com/spade-lang/swim/-/merge_requests/98
[!101]: https://gitlab.com/spade-lang/swim/-/merge_requests/101

## [0.3.0]

### Added

- [!89][!89]: Add the ability for plugins to define custom commands
- [!92][!92]: Write a list of libraries to `${BUILD_DIR}/libraries.json`

### Fixed

- [!84][!84]: The command `swim update-spade` has been fixed so it actually works.
- [!87][!87]: Stop rebuliding spade-python twice if changes occurred

### Changed

### Removed

### Internal

[!84]: https://gitlab.com/spade-lang/swim/-/merge_requests/84
[!87]: https://gitlab.com/spade-lang/swim/-/merge_requests/87
[!89]: https://gitlab.com/spade-lang/swim/-/merge_requests/89
[!92]: https://gitlab.com/spade-lang/swim/-/merge_requests/92

## [0.2.0] - 2023-04-20

- [!82][!82]*Breaking change*: Stop including stdlib and prelude in spade builds.

[!82]: https://gitlab.com/spade-lang/swim/-/merge_requests/82

## [0.1.0] - 2023-03-07

Initial numbered version

[Unreleased]: https://gitlab.com/spade-lang/swim/-/compare/v0.8.0...main
[0.8.0]: https://gitlab.com/spade-lang/swim/-/compare/v0.8.0...v0.7.0
[0.7.0]: https://gitlab.com/spade-lang/swim/-/compare/v0.7.0...v0.6.0
[0.6.0]: https://gitlab.com/spade-lang/swim/-/compare/v0.5.0...v0.6.0
[0.5.0]: https://gitlab.com/spade-lang/swim/-/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/spade-lang/swim/-/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/spade-lang/swim/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/spade-lang/swim/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/spade-lang/swim/-/tree/v0.1.0
